# kaldi-android

 ```
  allprojects {
  	repositories {
		...
		//github 
	  maven { url "https://raw.githubusercontent.com/louisgeek/kaldi-android/master" }
	  maven { url "https://gitlab.com/louisgeek/kaldi-android/-/raw/master" }
        
  	}
  }
  ```
  
  ```
  dependencies {
             implementation 'com.github.louisgeek:kaldi-android:5.2.0@aar'
  }
  ```
